package com.peterchappy.android.urbanbot;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.peterchappy.android.urbanbot.model.UrbanBotResponse;
import com.peterchappy.android.urbanbot.services.ServiceFactory;
import com.peterchappy.android.urbanbot.services.UrbanBotService;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.user_input)
    EditText mEditText;

    @Bind(R.id.word_title)
    TextView mTitle;

    @Bind(R.id.word_body)
    TextView mBody;

    @Bind(R.id.audio_grid_view)
    GridView mAudioGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //Creates an observer out of the EditText Field
        RxTextView.textChanges(mEditText)
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter(x -> x.length() > 2)
                .subscribe( x -> fetchUrbanBotData(x.toString()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchUrbanBotData(String s){

        UrbanBotService urbnService = ServiceFactory.createRetrofitService(UrbanBotService.class,
                UrbanBotService.WEB_SERVICE_BASE_URL);

        urbnService.getUser(s)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UrbanBotResponse>() {
                    @Override
                    public final void onCompleted() {
                        // do nothing
                    }

                    @Override
                    public final void onError(Throwable e) {
                        mBody.setText("JSON ERROR");
                    }

                    @Override
                    public final void onNext(UrbanBotResponse response) {
                        mTitle.setText(response.getTitle());
                        mBody.setText(response.getDefinition());
                        mAudioGrid.setAdapter(new AudioGridAdapter(getApplicationContext(), response.getAudio()));
                        }
                });
    }
}
