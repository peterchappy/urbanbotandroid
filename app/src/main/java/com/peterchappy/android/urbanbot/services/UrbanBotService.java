package com.peterchappy.android.urbanbot.services;

import com.peterchappy.android.urbanbot.model.UrbanBotResponse;

import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

public interface UrbanBotService {

    static final String WEB_SERVICE_BASE_URL = "http://api.peterchappy.com:9001";

    @GET("/api/{text}")
    Observable<UrbanBotResponse> getUser(@Path("text") String text);

}