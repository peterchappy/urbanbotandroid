package com.peterchappy.android.urbanbot;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.io.IOException;
import java.util.List;

public class AudioGridAdapter extends BaseAdapter {

    List<String> mAudioFiles;
    Context mContext;
    LayoutInflater mInflater;

    public AudioGridAdapter(Context c, List<String> a) {
        mAudioFiles = a;
        mContext = c;
        mInflater = (LayoutInflater) mContext.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mAudioFiles.size();
    }

    @Override
    public String getItem(int position) {
        return mAudioFiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.view_audio, parent, false);

        ViewHolder viewHolder = new ViewHolder(convertView);

        final MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnCompletionListener(v -> {
            viewHolder.pause.setVisibility(View.GONE);
            viewHolder.play.setVisibility(View.VISIBLE);
        });

        try {
            mediaPlayer.setDataSource(mAudioFiles.get(position));
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        viewHolder.play.setOnClickListener(v -> {
            Log.d("AUDIO: ", String.valueOf(position));
            viewHolder.play.setVisibility(View.GONE);
            viewHolder.pause.setVisibility(View.VISIBLE);
            mediaPlayer.start();
        });

        viewHolder.pause.setOnClickListener(v -> {
            viewHolder.pause.setVisibility(View.GONE);
            viewHolder.play.setVisibility(View.VISIBLE);
            mediaPlayer.pause();
        });

        return convertView;
    }

    public class ViewHolder {

        ImageView pause, play;

        public ViewHolder(View v) {
            play = (ImageView) v.findViewById(R.id.audio_command_play);
            pause = (ImageView) v.findViewById(R.id.audio_command_pause);
        }
    }
}
