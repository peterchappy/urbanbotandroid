
package com.peterchappy.android.urbanbot.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UrbanBotResponse {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("audio")
    @Expose
    private List<String> audio = new ArrayList<String>();
    @SerializedName("query")
    @Expose
    private String query;
    @SerializedName("definition")
    @Expose
    private String definition;

    /**
     * No args constructor for use in serialization
     *
     */
    public UrbanBotResponse() {
    }

    /**
     *
     * @param title
     * @param definition
     * @param audio
     * @param query
     */
    public UrbanBotResponse(String title, List<String> audio, String query, String definition) {
        this.title = title;
        this.audio = audio;
        this.query = query;
        this.definition = definition;
    }

    /**
     *
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     *     The audio
     */
    public List<String> getAudio() {
        return audio;
    }

    /**
     *
     * @param audio
     *     The audio
     */
    public void setAudio(List<String> audio) {
        this.audio = audio;
    }

    /**
     *
     * @return
     *     The query
     */
    public String getQuery() {
        return query;
    }

    /**
     *
     * @param query
     *     The query
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     *
     * @return
     *     The definition
     */
    public String getDefinition() {
        return definition;
    }

    /**
     *
     * @param definition
     *     The definition
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }

}
